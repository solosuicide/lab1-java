package ua.kpi.fpm.pzks.fs;

import java.util.concurrent.Semaphore;

public class LogFile extends Entity {
    private StringBuffer data;

    private LogFile(String name, Directory parent, String data) {
        super(name, parent);
        this.data = new StringBuffer(data);
    }

    public static LogFile create(String name, Directory parent, String data) {
        return new LogFile(name, parent, data);
    }

    public String read() {
        return data.toString();
    }

    public void append(String line) {
        data.append(line);
    }
}
