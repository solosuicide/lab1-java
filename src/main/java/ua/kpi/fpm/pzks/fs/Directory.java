package ua.kpi.fpm.pzks.fs;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;

public class Directory extends Entity {
    private static int MAX_ELEMS = 10;
    private Map<String, Entity> childs = new Hashtable<>();
    private final Object monitor = new Object();

    protected Directory(String name, Directory parent) {
        super(name, parent);
    }

    private Directory() {
        super("root");
    }

    public static Directory create(String name, Directory parent) {
        if (parent == null) {
            parent = new Directory();
        }
        return new Directory(name, parent);
    }

    public boolean containsChild(String name) {
        return childs.containsKey(name);
    }

    public void addChild(Entity child) {
        synchronized (monitor) {
            if (childs.size() <= MAX_ELEMS)
                childs.put(child.getName(), child);
        }

    }

    @Override
    protected void move(Directory destination) {
        super.move(destination);
    }

    @Override
    protected void delete(String name) {
        childs.remove(name);
    }

    public List<Entity> list() {
        synchronized (monitor) {

        }
        return null;
    }
}
